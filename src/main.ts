import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import {
  APP_PORT,
  EVENTS_HOST,
  EVENTS_PASSWORD,
  EVENTS_PORT,
  EVENTS_PROTO,
  EVENTS_USER,
} from './constants/config-options';
import { WatchJobsService } from './kubectl/watch/watch-jobs/watch-jobs.service';
import { setupSwagger } from './swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  const config = app.get(ConfigService);

  const eventsHost = config.get(EVENTS_HOST);
  const eventsPort = config.get(EVENTS_PORT);
  const eventsPassword = config.get(EVENTS_PASSWORD);
  const eventsProto = config.get(EVENTS_PROTO);
  const eventsUser = config.get(EVENTS_USER);

  if (eventsHost && eventsPassword && eventsPort && eventsProto && eventsUser) {
    const watchJobs = app.get(WatchJobsService);
    app.init().then(() => watchJobs.start());
  }

  const port = Number(config.get(APP_PORT));
  setupSwagger(app);
  await app.listen(port);
}
bootstrap();
