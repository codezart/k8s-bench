import { Controller, Get } from '@nestjs/common';
import { AppService } from './app.service';
import { OK } from './constants/app-constants';

@Controller()
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get()
  getServiceName() {
    return this.appService.getServiceName();
  }

  @Get('api/healthz')
  healthCheck() {
    return { status: OK };
  }
}
