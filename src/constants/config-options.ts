import * as Joi from '@hapi/joi';
import { ConfigModuleOptions } from '@nestjs/config/dist/interfaces';

export const ENV_PATH = '.env';

export const NODE_ENV = 'NODE_ENV';
export const ERPNEXT_VERSION = 'ERPNEXT_VERSION';
export const ERPNEXT_NAMESPACE = 'ERPNEXT_NAMESPACE';
export const PVC_NAME = 'PVC_NAME';
export const ERPNEXT_HELM_RELEASE = 'ERPNEXT_HELM_RELEASE';
export const BENCH_UID = 'BENCH_UID';
export const BENCH_SECRET = 'BENCH_SECRET';
export const APP_PORT = 'APP_PORT';
export const PY_IMAGE = 'PY_IMAGE';
export const NGINX_IMAGE = 'NGINX_IMAGE';
export const MARIADB_SECRET_NAME = 'MARIADB_SECRET_NAME';
export const DELETE_SITE_CONFIG_MAP = 'DELETE_SITE_CONFIG_MAP';
export const WIZARD_SCRIPT_CONFIG_MAP = 'WIZARD_SCRIPT_CONFIG_MAP';
export const UPGRADE_SITE_CONFIG_MAP = 'UPGRADE_SITE_CONFIG_MAP';
export const IMAGE_PULL_SECRET_NAME = 'IMAGE_PULL_SECRET_NAME';
export const DB_ROOT_USER = 'DB_ROOT_USER';

// Optional Events
export const EVENTS_HOST = 'EVENTS_HOST';
export const EVENTS_PORT = 'EVENTS_PORT';
export const EVENTS_PASSWORD = 'EVENTS_PASSWORD';
export const EVENTS_PROTO = 'EVENTS_PROTO';
export const EVENTS_USER = 'EVENTS_USER';
export const EVENTS_CLIENT_ID = 'EVENTS_CLIENT_ID';

export const configOptions: ConfigModuleOptions = {
  isGlobal: true,
  envFilePath: process.env.ENV_PATH || ENV_PATH,
  validationSchema: Joi.object({
    [NODE_ENV]: Joi.string()
      .valid('development', 'production', 'test', 'staging')
      .default('development'),
    [ERPNEXT_VERSION]: Joi.string().required(),
    [ERPNEXT_NAMESPACE]: Joi.string().required(),
    [PVC_NAME]: Joi.string().required(),
    [ERPNEXT_HELM_RELEASE]: Joi.string().required(),
    [BENCH_UID]: Joi.string().required(),
    [BENCH_SECRET]: Joi.string().required(),
    [APP_PORT]: Joi.string().default('7000'),
    [PY_IMAGE]: Joi.string().default('frappe/erpnext-worker'),
    [NGINX_IMAGE]: Joi.string().default('frappe/erpnext-nginx'),
    [MARIADB_SECRET_NAME]: Joi.string().default('mariadb-root-password'),
    [DELETE_SITE_CONFIG_MAP]: Joi.string().default('delete-site'),
    [WIZARD_SCRIPT_CONFIG_MAP]: Joi.string().default('wizard-script'),
    [UPGRADE_SITE_CONFIG_MAP]: Joi.string().default('upgrade-site'),
    [DB_ROOT_USER]: Joi.string().default('root'),
    [IMAGE_PULL_SECRET_NAME]: Joi.string().optional().allow('', null),
    [EVENTS_HOST]: Joi.string().optional().allow('', null),
    [EVENTS_PORT]: Joi.string().optional().allow('', null),
    [EVENTS_PASSWORD]: Joi.string().optional().allow('', null),
    [EVENTS_PROTO]: Joi.string().optional().allow('', null),
    [EVENTS_USER]: Joi.string().optional().allow('', null),
    [EVENTS_CLIENT_ID]: Joi.string().optional().allow('', null),
  }),
};
