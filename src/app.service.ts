import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { SERVICE_NAME } from './constants/app-constants';
import { BENCH_UID } from './constants/config-options';

@Injectable()
export class AppService {
  constructor(private readonly config: ConfigService) {}

  getServiceName(): { service: string } {
    const config = {
      uuid: this.config.get(BENCH_UID),
    };

    return { service: SERVICE_NAME, ...config };
  }
}
