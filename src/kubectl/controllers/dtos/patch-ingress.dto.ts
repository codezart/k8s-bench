import { ApiProperty } from '@nestjs/swagger';
import { IsFQDN, IsString } from 'class-validator';

export class PatchIngressDto {
  @IsFQDN()
  @ApiProperty()
  siteName: string;

  @IsString()
  @ApiProperty()
  service: string;
}
