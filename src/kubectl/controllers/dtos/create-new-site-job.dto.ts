import { ApiProperty } from '@nestjs/swagger';
import { IsString, IsOptional, IsUUID, IsEmail, IsFQDN } from 'class-validator';

export class CreateNewSiteJobDto {
  @IsFQDN()
  @ApiProperty()
  siteName: string;

  @IsUUID()
  @ApiProperty()
  benchUid: string;

  @IsString({ each: true })
  @ApiProperty({ required: false })
  apps: string[];

  @IsOptional()
  @IsString()
  @ApiProperty({ required: false })
  language: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ required: false })
  country: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ required: false })
  timezone: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ required: false })
  fullName: string;

  @IsOptional()
  @IsEmail()
  @ApiProperty({ required: false })
  email: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ required: false })
  password: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ required: false })
  wildcardDomain: string;

  @IsOptional()
  @IsString()
  @ApiProperty({ required: false })
  wildcardTlsSecretName: string;

  @IsEmail()
  @ApiProperty()
  owner: string;
}
