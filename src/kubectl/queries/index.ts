import { GetIngressHandler } from './get-ingress/get-ingress.handler';
import { GetJobStatusHandler } from './get-job-status/get-job-status.handler';

export const KubeQueryHandlers = [GetJobStatusHandler, GetIngressHandler];
