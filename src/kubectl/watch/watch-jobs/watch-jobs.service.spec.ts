import { ConfigService } from '@nestjs/config';
import { Test, TestingModule } from '@nestjs/testing';
import { BROADCAST_EVENT } from '../../../events-microservice.client';
import { KubectlService } from '../../aggregates/kubectl/kubectl.service';
import { WatchJobsService } from './watch-jobs.service';

describe('WatchJobsService', () => {
  let service: WatchJobsService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        WatchJobsService,
        { provide: BROADCAST_EVENT, useValue: {} },
        { provide: KubectlService, useValue: {} },
        { provide: ConfigService, useValue: {} },
      ],
    }).compile();

    service = module.get<WatchJobsService>(WatchJobsService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
