import { HttpModule } from '@nestjs/axios';
import { Module } from '@nestjs/common';
import { CqrsModule } from '@nestjs/cqrs';
import { ClientsModule } from '@nestjs/microservices';
import { TerminusModule } from '@nestjs/terminus';
import { eventsClient } from '../events-microservice.client';
import { KubeAggregates } from './aggregates';
import { KubeCommandHandlers } from './commands';
import { KubeController } from './controllers/kube/kube.controller';
import { KubeEventHandlers } from './events';
import { IngressConfigProvider } from './providers/ingress-config.provider';
import { KubeQueryHandlers } from './queries';
import { KubeWatchServices } from './watch';

@Module({
  imports: [
    CqrsModule,
    TerminusModule,
    HttpModule,
    ClientsModule.registerAsync([eventsClient]),
  ],
  controllers: [KubeController],
  providers: [
    ...KubeEventHandlers,
    ...KubeCommandHandlers,
    ...KubeAggregates,
    ...KubeQueryHandlers,
    ...KubeWatchServices,
    IngressConfigProvider,
  ],
})
export class KubectlModule {}
