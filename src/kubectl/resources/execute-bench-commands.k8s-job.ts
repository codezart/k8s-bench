import { V1Job } from '@kubernetes/client-node';
import { ASSETS_CACHE, SITES_DIR } from '../../constants/app-constants';

export const EXECUTE_BENCH_COMMANDS = 'execute-bench-commands';
export const RUN_WIZARD = 'run-wizard';
export const POPULATE_ASSETS = 'populate-assets';

export const executeBenchCommandJobJsonTemplate = (
  jobName: string,
  namespace: string,
  version: string,
  pvc: string,
  commands: string[],
  options: string[],
  workerImage: string,
  imagePullSecretName?: string,
  areAssetsRequired?: boolean,
  isNewImage?: boolean,
): V1Job => {
  const job: V1Job = {
    metadata: {
      name: jobName,
      namespace,
    },
    spec: {
      backoffLimit: 0,
      template: {
        spec: {
          imagePullSecrets: imagePullSecretName
            ? [{ name: imagePullSecretName }]
            : [],
          securityContext: {
            supplementalGroups: [1000],
          },
          containers: [
            {
              name: RUN_WIZARD,
              image: `${workerImage}:${version}`,
              command: commands.length ? commands : ['bench'],
              args: options.length ? options : [],
              imagePullPolicy: 'IfNotPresent',
              volumeMounts: [
                {
                  name: SITES_DIR,
                  mountPath: '/home/frappe/frappe-bench/sites',
                },
              ],
            },
          ],
          restartPolicy: 'Never',
          volumes: [
            {
              name: SITES_DIR,
              persistentVolumeClaim: {
                claimName: pvc,
                readOnly: false,
              },
            },
          ],
        },
      },
    },
  };

  if (areAssetsRequired) {
    const command = [];
    const args = [];
    const volumeMounts = [];
    if (isNewImage) {
      command.push('cp', '-fR');
      args.push(
        '/usr/share/nginx/html/assets',
        '/home/frappe/frappe-bench/sites',
      );
      volumeMounts.push({
        name: ASSETS_CACHE,
        mountPath: '/home/frappe/frappe-bench/sites/assets',
      });
    } else {
      command.push('bash', '-c');
      args.push('rsync', '-a', '--delete', '/var/www/html/assets', '/assets');
      volumeMounts.push({
        name: ASSETS_CACHE,
        mountPath: '/assets',
      });
    }
    job.spec.template.spec.initContainers = [
      {
        name: POPULATE_ASSETS,
        image: `${workerImage}:${version}`,
        command,
        args,
        volumeMounts,
      },
    ];

    job.spec.template.spec.volumes.push({
      name: ASSETS_CACHE,
      emptyDir: {},
    });
  }

  return job;
};
