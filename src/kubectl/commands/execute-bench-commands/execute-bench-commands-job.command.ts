import { ICommand } from '@nestjs/cqrs';
import { Request } from 'express';
import { ExecuteBenchCommandsDto } from '../../controllers/dtos/execute-bench-commands.dto';

export class ExecuteBenchCommandsCommand implements ICommand {
  constructor(
    public readonly payload: ExecuteBenchCommandsDto,
    public readonly request: Request & { idToken: unknown },
  ) {}
}
