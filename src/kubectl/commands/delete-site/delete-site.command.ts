import { ICommand } from '@nestjs/cqrs';
import { Request } from 'express';
import { DeleteSiteJobDto } from '../../controllers/dtos/delete-site-job.dto';

export class DeleteSiteCommand implements ICommand {
  constructor(
    public readonly payload: DeleteSiteJobDto,
    public readonly request: Request & { idToken: unknown },
  ) {}
}
