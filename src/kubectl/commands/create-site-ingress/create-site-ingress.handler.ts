import { CommandHandler, ICommandHandler, EventPublisher } from '@nestjs/cqrs';
import { CreateSiteIngressCommand } from './create-site-ingress.command';
import { CreateIngressAggregateService } from '../../aggregates/create-ingress-aggregate/create-ingress-aggregate.service';
import { lastValueFrom } from 'rxjs';

@CommandHandler(CreateSiteIngressCommand)
export class CreateSiteIngressHandler implements ICommandHandler {
  constructor(
    private readonly publisher: EventPublisher,
    private readonly manager: CreateIngressAggregateService,
  ) {}

  async execute(command: CreateSiteIngressCommand) {
    const {
      svcName,
      jobName,
      namespace,
      wildcardDomain,
      wildcardTlsSecretName,
    } = command;
    const aggregate = this.publisher.mergeObjectContext(this.manager);
    await lastValueFrom(
      aggregate.createIngress(
        jobName,
        namespace,
        svcName,
        wildcardDomain,
        wildcardTlsSecretName,
      ),
    );
    aggregate.commit();
  }
}
