import { ICommand } from '@nestjs/cqrs';
import { UpgradeSiteDto } from '../../controllers/dtos/upgrade-site-job.dto';

export class UpgradeSiteCommand implements ICommand {
  constructor(public readonly payload: UpgradeSiteDto) {}
}
