import { ICommand } from '@nestjs/cqrs';
import { Request } from 'express';
import { CreateNewSiteJobDto } from '../../controllers/dtos/create-new-site-job.dto';

export class CreateNewSiteJobCommand implements ICommand {
  constructor(
    public readonly payload: CreateNewSiteJobDto,
    public readonly request: Request & { idToken: unknown },
  ) {}
}
