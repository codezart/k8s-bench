import { ICommand } from '@nestjs/cqrs';

export class DeleteSiteResourcesCommand implements ICommand {
  constructor(public readonly siteName: string) {}
}
