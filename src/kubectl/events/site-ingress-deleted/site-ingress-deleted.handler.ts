import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { SiteIngressDeletedEvent } from './site-ingress-deleted.event';

@EventsHandler(SiteIngressDeletedEvent)
export class SiteIngressDeletedHandler implements IEventHandler {
  constructor() {}

  handle(event: SiteIngressDeletedEvent) {
    // TODO: Fire Webhook
    return { ingressName: event.ingressName };
  }
}
