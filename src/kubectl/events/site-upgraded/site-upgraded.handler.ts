import { EventsHandler, IEventHandler } from '@nestjs/cqrs';
import { InternalServerErrorException, Logger } from '@nestjs/common';
import * as k8s from '@kubernetes/client-node';
import { KubectlService } from '../../aggregates/kubectl/kubectl.service';
import { SiteUpgradedEvent } from './site-upgraded.event';

@EventsHandler(SiteUpgradedEvent)
export class SiteUpgradedHandler implements IEventHandler {
  constructor(private readonly kube: KubectlService) {}

  async handle(event: SiteUpgradedEvent) {
    const { namespace, job } = event;
    const batchV1Api = this.kube.k8sConfig.makeApiClient(k8s.BatchV1Api);
    try {
      await batchV1Api.createNamespacedJob(namespace, job);
    } catch (error) {
      Logger.error(error, error.toString(), this.constructor.name);
      throw new InternalServerErrorException(error);
    }
  }
}
