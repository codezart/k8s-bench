import {
  BadGatewayException,
  BadRequestException,
  Injectable,
} from '@nestjs/common';
import { AggregateRoot } from '@nestjs/cqrs';
import { ConfigService } from '@nestjs/config';
import * as k8s from '@kubernetes/client-node';

import { UpgradeSiteDto } from '../../controllers/dtos/upgrade-site-job.dto';
import { generateUpdateSiteJobJsonTemplate } from '../../resources/upgrade-site.k8s-job';
import {
  BENCH_UID,
  ERPNEXT_NAMESPACE,
  ERPNEXT_VERSION,
  IMAGE_PULL_SECRET_NAME,
  NGINX_IMAGE,
  PVC_NAME,
  PY_IMAGE,
  UPGRADE_SITE_CONFIG_MAP,
} from '../../../constants/config-options';
import { SiteUpgradedEvent } from '../../events/site-upgraded/site-upgraded.event';
import { from } from 'rxjs';
import { switchMap } from 'rxjs/operators';
import { KubectlService } from '../kubectl/kubectl.service';
import { CONTENT_TYPE } from '../../../constants/app-constants';

@Injectable()
export class UpgradeSiteAggregateService extends AggregateRoot {
  constructor(
    private readonly config: ConfigService,
    private readonly kube: KubectlService,
  ) {
    super();
  }

  async upgradeSite(payload: UpgradeSiteDto) {
    const namespace = this.config.get(ERPNEXT_NAMESPACE);
    const pvc = this.config.get(PVC_NAME);
    const version = this.config.get(ERPNEXT_VERSION);
    const pyImage = this.config.get(PY_IMAGE);
    const nginxImage = this.config.get(NGINX_IMAGE);
    const benchUid = this.config.get(BENCH_UID);
    const upgradeSiteCM = this.config.get(UPGRADE_SITE_CONFIG_MAP);
    const imagePullSecretName = this.config.get(IMAGE_PULL_SECRET_NAME);
    const job = generateUpdateSiteJobJsonTemplate(
      payload,
      namespace,
      pyImage,
      nginxImage,
      pvc,
      benchUid,
      version,
      upgradeSiteCM,
      imagePullSecretName,
    );

    if (
      namespace &&
      pvc &&
      version &&
      pyImage &&
      nginxImage &&
      benchUid &&
      job
    ) {
      this.apply(new SiteUpgradedEvent(namespace, job));
      return { namespace, job };
    }

    throw new BadGatewayException({ namespace, job });
  }

  async patchIngress(siteName: string, svc: string) {
    const namespace = this.config.get(ERPNEXT_NAMESPACE);

    if (siteName && namespace) {
      const netV1Api = this.kube.k8sConfig.makeApiClient(k8s.NetworkingV1Api);
      return await from(netV1Api.readNamespacedIngress(siteName, namespace))
        .pipe(
          switchMap(ingress => {
            const patch = ingress.body;
            if (patch?.spec?.rules?.length > 0) {
              if (patch?.spec?.rules[0].http.paths.length > 0) {
                patch.spec.rules[0].http.paths[0].backend.service.name = svc;
              }
            }
            const options = {
              headers: {
                [CONTENT_TYPE]:
                  k8s.PatchUtils.PATCH_FORMAT_STRATEGIC_MERGE_PATCH,
              },
            };

            return from(
              netV1Api.patchNamespacedIngress(
                siteName,
                namespace,
                patch,
                undefined,
                undefined,
                undefined,
                undefined,
                options,
              ),
            );
          }),
        )
        .toPromise();
    }

    throw new BadRequestException({
      IngressPatchFailed: siteName,
    });
  }
}
