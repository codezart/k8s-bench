import { Global, Module } from '@nestjs/common';
import { BenchGuard } from './guards/bench/bench.guard';

@Global()
@Module({
  providers: [BenchGuard],
  exports: [BenchGuard],
})
export class AuthModule {}
